import multimatch from 'multimatch'
import debug from 'debug'
import {
  sortBy,
  reverse
} from 'lodash'

const dbg = debug(`metalsmith-collections`)

export default function (operations) {
  return function collections (files, metalsmith) {
    if (!Array.isArray(operations)) operations = [operations]
    operations.forEach((op) => {
      const {mask, key, sortKey, sortOrder, limit} = op
      let paths
      if (typeof mask === 'function') paths = mask(files)
      else paths = multimatch(Object.keys(files), mask)
      let collection = []
      metalsmith.metadata()[key] = collection
      if (sortKey) paths = sortBy(paths, (path) => files[path][sortKey])
      if (sortOrder === 'desc') paths = reverse(paths)
      if (limit) paths = paths.slice(0, limit)
      paths.forEach((path) => collection.push(files[path]))
      dbg(`${paths.length} files added to ${key}`)
    })
  }
}
