import multimatch from 'multimatch'
import debug from 'debug'
import request from 'superagent'
import config from 'config'
import {
  ValueCache
} from 'metalsmith-cache'

const dbg = debug(`metalsmith-unsplash`)

export default function (mask, options = {}) {
  if (!Array.isArray(mask)) mask = [mask]
  const cache = new ValueCache('unsplash')
  return async function unsplash (files) {
    const paths = multimatch(Object.keys(files), mask)
    let countApi = 0
    let countCache = 0
    while (paths.length) {
      const file = files[paths.shift()]
      if (!file.unsplashCoverId) continue
      try {
        file.unsplashCover = await cache.retrieve(file.unsplashCoverId)
        countCache++
      } catch (err) {
        try {
          const auth = `Client-ID ${config.get('unsplash.accessKey')}`
          const res = await request
          .get(`https://api.unsplash.com/photos/${file.unsplashCoverId}`)
          .set(`Authorization`, auth)
          .set('accept', 'json')
          file.unsplashCover = res.body
          cache.store(file.unsplashCoverId, file.unsplashCover)
          countApi++
        } catch (err) {
          dbg('something went wrong with unsplash request')
          dbg(err)
        }
      }
      file.cover = file.unsplashCover.urls.full
    }
    dbg(`cache:${countCache} api:${countApi} total:${countCache + countApi}`)
  }
}
