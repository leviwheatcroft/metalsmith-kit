import '../styles/cleanBlog.less'
import Schnack from 'schnack/build/client.js'
// import commentsLoader from '../../layouts/cleanBlog/commentsLoader'

// Floating label headings for the contact form
$(function () {
  $('body').on('input propertychange', '.floating-label-form-group', function (e) {
    $(this).toggleClass('floating-label-form-group-with-value', !!$(e.target).val())
  }).on('focus', '.floating-label-form-group', function () {
    $(this).addClass('floating-label-form-group-with-focus')
  }).on('blur', '.floating-label-form-group', function () {
    $(this).removeClass('floating-label-form-group-with-focus')
  })
})

// Navigation Scripts to Show Header on Scroll-Up
jQuery(document).ready(function ($) {
  var MQL = 1170

  // primary navigation slide-in effect
  if ($(window).width() > MQL) {
    var headerHeight = $('.navbar-custom').height()
    $(window).on('scroll', { previousTop: 0 }, () => {
      var currentTop = $(window).scrollTop()
      // check if user is scrolling up
      if (currentTop < this.previousTop) {
        // if scrolling up...
        if (
          currentTop > 0 &&
          $('.navbar-custom').hasClass('is-fixed')
        ) {
          $('.navbar-custom').addClass('is-visible')
        } else {
          $('.navbar-custom').removeClass('is-visible is-fixed')
        }
      } else if (currentTop > this.previousTop) {
        // if scrolling down...
        $('.navbar-custom').removeClass('is-visible')
        if (
          currentTop > headerHeight &&
          !$('.navbar-custom').hasClass('is-fixed')
        ) {
          $('.navbar-custom').addClass('is-fixed')
        }
      }
      this.previousTop = currentTop
    })
  }
})

// ekko light box
$(document).on('click', '[data-toggle="lightbox"]', function (event) {
  event.preventDefault()
  $(this).ekkoLightbox()
})

$(document).on('click', '#comments-load', function (event) {
  $('#comments-load').addClass('loading')

  // eslint-disable-next-line no-new
  new Schnack({
    target: '#comments-container',
    slug: $(this).data('slug'),
    host: 'https://comments.leviwheatcroft.com',
    partials: {
      Preview: `Preview`,
      Edit: `Edit`,
      SendComment: `Send comment`,
      Cancel: `Cancel`,
      Or: ``,
      Mute: `mute notifications`,
      UnMute: `unmute`,
      PostComment: `Post a comment. Markdown is supported!`,
      AdminApproval: `This comment is still waiting for your approval`,
      WaitingForApproval: `Your comment is still waiting for approval by the site owner`,
      SignInVia: `To post a comment you need to sign in with one of these providers:`,
      Reply: `<i class='icon schnack-icon-reply'></i> reply`,
      LoginStatus: "(signed in as <span class='schnack-user'>@%USER%</span> :: <a class='schnack-signout' href='#'>sign out</a>)"
    }
  })
})
