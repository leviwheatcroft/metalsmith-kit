import copy from 'copy'
import debug from 'debug'

const dbg = debug('metalsmith-levistat')

copy('site/resources/**/*', 'build', (err, file) => {
  if (err) console.error(err)
  dbg(`buildResources done (${file.length} files)`)
})
