import collections from './collections'
import move from 'metalsmith-move'
import tags from 'metalsmith-tags'
import mapsite from 'metalsmith-mapsite'
import transform from './transform'
import unsplash from './unsplash'
import report from './report'
import multimatch from 'multimatch'
import {
  parse
} from 'path'
import moment from 'moment'
import slug from 'slug'
import debug from 'debug'

// eslint-disable-next-line
const dbg = debug('metalsmith-levistat')
const validStatus = [
  'published',
  'archived'
]

const plugins = [

  // move files to where they will end up, right from the start
  // don't worry about keeping the .md extension
  // don't try to set path according to some meta like "slug" or whatever
  function moveFiles (files) {
    const images = multimatch(Object.keys(files), 'articles/**/*.+(png|jpg)')
    images.forEach((path) => {
      const file = files[path]
      files[`images/${parse(path).base}`] = file
      delete files[path]
    })
    dbg(`moved ${images.length} images`)

    const articles = multimatch(Object.keys(files), 'articles/**/*.md')
    articles.forEach((path) => {
      const file = files[path]
      files[`articles/${slug(file.title, {lower: true})}.html`] = file
      delete files[path]
    })
    dbg(`moved ${articles.length} articles`)
  },

  // ignore files which aren't published
  function ignoreUnpublished (files) {
    const length = Object.keys(files).length
    const articles = multimatch(Object.keys(files), 'articles/*')
    articles.forEach((path) => {
      if (!validStatus.includes(files[path].status)) {
        dbg(`ignoring ${path} (status: ${files[path].status})`)
        delete files[path]
      }
    })
    dbg(`ignored ${length - Object.keys(files).length} files`)
  },

  // set meta on articles
  function setArticleMeta (files) {
    const articles = multimatch(Object.keys(files), 'articles/*')
    articles.forEach((path) => {
      const file = files[path]
      const date = moment(file.updated || file.published, 'DD/MM/YY')
      file.displayDate = date.format('MMMM Do YYYY')
      file.lastmod = date.format('YYYY-MM-DD')
      if (file.canonicalHost) {
        const canonicalPath = path.replace(parse(path).ext, '')
        file.canonical = `https://${file.canonicalHost}/${canonicalPath}`
      }
      file.path = path
    })
    dbg(`updated meta on ${articles.length} files`)
  },

  // populate covers from unsplash
  unsplash('articles/*'),

  collections([
    {
      key: 'articles',
      mask: [ 'articles/*', '!articles/index' ],
      sortKey: 'lastmod',
      sortOrder: 'desc'
    }
  ]),

  tags({
    path: 'tags/:tag.html'
  }),

  transform([
    {
      mask: ['articles/*'],
      layout: 'cleanBlog/post'
    },
    {
      mask: ['tags/*'],
      layout: 'cleanBlog/tag'
    },
    {
      // no layout specified here, read layout from file meta
      mask: ['archive.pug', 'index.pug']
    }
  ]),

  // rename things, don't move here
  move({
    '**/*.md': '{dir}/{name}.html',
    '**/*.pug': '{dir}/{name}.html'
  }),

  mapsite({
    hostname: 'https://leviwheatcroft.com',
    pattern: [
      '**/*.html'
    ],
    omitExtension: true
  }),

  report()

].filter((e) => e) // remove disabled plugins

export default plugins
