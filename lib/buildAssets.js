import webpack from 'webpack'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin'
// import HashPlugin from 'hash-webpack-plugin'
import debug from 'debug'

import {
  resolve
} from 'path'
import UglifyJsPlugin from 'uglifyjs-webpack-plugin'
// import {
//   get
// } from 'http'

const dbg = debug('metalsmith-build-assets')

const publicPath = resolve(__dirname, '../build')

const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development'

// optimization, see: https://github.com/webpack-contrib/mini-css-extract-plugin
const compiler = webpack(
  {
    mode,
    entry: {
      cleanBlog: './site/assets/js/cleanBlog.js',
      resume: './site/assets/js/resume.js'
    },
    output: {
      path: publicPath,
      filename: 'js/[name].js'
    },
    resolve: {
      extensions: ['.js', '.less', '.pug']
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'styles/[name].css',
        chunkFilename: '[id].[hash].css'
      })
      // new HashPlugin({ path: publicPath, fileName: 'hash.txt' })
      // new webpack.ProvidePlugin({
      //   $: 'jquery',
      //   jQuery: 'jquery',
      //   'window.jQuery': 'jquery'
      // })
    ],
    optimization: {
      // minimize: false,
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          sourceMap: false // set to true if you want JS source maps
        }),
        new OptimizeCSSAssetsPlugin({})
      ]
    },
    module: {
      rules: [
        // use expose-loader instead of webpack.ProvidePlugin
        // this allows external components to use the same instance of jquery
        // which gives them access to $().component() defined in Component
        {
          test: require.resolve('jquery'),
          use: [
            {
              loader: 'expose-loader',
              options: 'jQuery' // paste.js expects `window.jQuery`
            },
            {
              loader: 'expose-loader',
              options: '$'
            }
          ]
        },
        {
          test: /\.(c|le)ss$/,
          use: [
            { loader: MiniCssExtractPlugin.loader },
            {
              loader: 'css-loader',
              options: {
                modules: false // better to specify what you want locally scoped
              }
            },
            { loader: 'less-loader' } // compiles Less to CSS
          ]
        },
        {
          test: /\.pug/,
          use: [
            { loader: 'pug-loader' }
          ]
        },
        {
          test: /\.csv$/,
          loader: 'csv-loader',
          options: {
            dynamicTyping: true,
            header: true,
            skipEmptyLines: true
          }
        },
        mode === 'development' ? {} : {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', {
                  // defined in package.json
                  // targets: '> 0.25%, not dead'
                }]
              ]
            }
          }
        }
      ]
    }
  }
)

compiler.run((err, stats) => {
  if (err) dbg(err)
  else if (stats.hasErrors()) dbg(stats.compilation.errors)
  // trigger browsersync reload
  else {
    dbg(`webpack looks ok: ${stats.hash}`)
  }
})

// export default function buildAssets (callback) {
//   compiler.run((err, stats) => {
//     if (err) dbg(err)
//     else if (stats.hasErrors()) dbg(stats.compilation.errors)
//     // trigger browsersync reload
//     else {
//       dbg(`webpack looks ok: ${stats.hash}`)
//     }
//     callback()
//   })
// }
// compiler.run((err, stats) => {
//   if (err) dbg(err)
//   else if (stats.hasErrors()) dbg(stats.compilation.errors)
//   // trigger browsersync reload
//   else {
//     dbg(`webpack looks ok: ${stats.hash}`)
//   }
//   if (process.env.NODE_ENV === 'dev') {
//     dbg('assets (re)built, request browser reload')
//     get('http://localhost:3000/__browser_sync__?method=reload')
//     .on('error', () => console.log('browser-sync not listening'))
//   }
// })
