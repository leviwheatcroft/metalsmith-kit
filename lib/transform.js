import {
  render
} from 'pug'
import marked from 'marked'
import {
  join
} from 'path'
import {
  readFileSync
} from 'fs'
import multimatch from 'multimatch'
import debug from 'debug'
const dbg = debug('metalsmith-transform')

function cleanUrl (url) {
  return url.replace(/\.(md|html|htm)/, '')
}

const markedOptions = {}
markedOptions.renderer = new marked.Renderer()
markedOptions.renderer.image = function (href, title, text) {
  return `
      <a href="${href}", data-toggle="lightbox">
        <img src="${href}" class="figure-img img-responsive rounded" alt="${text}">
      </a>
    `
}

function markdown (contents) {
  return marked(contents, markedOptions)
}

const basedir = 'site/layouts'
const layouts = {}

export default function (operations) {
  return function transform (files, metalsmith) {
    operations.forEach((op) => {
      const paths = multimatch(Object.keys(files), op.mask)
      paths.forEach((path) => {
        dbg(`rendering ${path}`)
        const file = files[path]
        if (!op.layout && !file.layout) {
          throw new Error(`no layout specified: ${path}`)
        }
        const layout = join(basedir, (op.layout || file.layout) + '.pug')
        try {
          if (!layouts[layout]) layouts[layout] = readFileSync(layout, 'utf8')
        } catch (err) {
          throw new Error(`bad layout specified: ${path} (${layout})`)
        }
        try {
          const locals = Object.assign(
            {},
            metalsmith.metadata(),
            file,
            {
              cleanUrl,
              markdown,
              contents: file.contents.toString(),
              basedir // this is an option for pug
            }
          )
          file.contents = Buffer.from(render(layouts[layout], locals))
        } catch (err) {
          dbg(`error rendering ${path}`)
          dbg(err)
        }
      })
    })
  }
}
