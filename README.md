## Things to Check

 - check tag pages work, links to tag pages correct, links on tab pages correct
 - check all files are generated with .html extensions
 - check canonical urls look correct


## Install Schnack for Comments

install node
```
apt-get install build-essential
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs
```

install pm2
```
adduser pm2
usermod -aG sudo pm2
su -l pm2
echo "PATH=$PATH:/home/pm2/node_modules/bin" >> ~/.bashrc
source ~/.bashrc
npm config set prefix /home/pm2/node_modules
npm i -g pm2
```

install schnack
```
cd /srv
git clone https://github.com/schn4ck/schnack
cd schnack
npm i
cp config.tpl.json config.json
nano config.json
sudo chown -R pm2:pm2 /srv/schnack
```


first start
```
cd /srv/schnack
pm2 init
pm2 start index.js
p2m startup
```
well echo a command you need to copy & paste

nginx
```
apt-get install nginx
nano /etc/nginx/conf.d/comments.leviwheatcroft.com.conf
```

comments.leviwheatcroft.com.conf
```
server {
  server_name comments.leviwheatcroft.com;
  # location / {
  #   proxy_pass http://localhost:3000/;
  # }
}
```

make sure dns for comments.leviwheatcroft.com points to instance, then
```
certbot
```

then uncomment the commented lines in comments.leviwheatcroft.com.conf and
```
systemctl restart nginx
```
