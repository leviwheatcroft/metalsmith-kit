import multimatch from 'multimatch'
import moment from 'moment'
import debug from 'debug'

const dbg = debug(`metalsmith-report`)

export default function () {
  return function report (files) {
    const articles = multimatch(Object.keys(files), 'articles/*')

    // all articles should have lastmod, should be in form 'YYYY-MM-DD'
    articles.forEach((path) => {
      const file = files[path]
      if (!file.lastmod) {
        return dbg(`${path} : 'lastmod' not set`)
      }
      if (!moment(file.lastmod, 'YYYY-MM-DD').isValid()) {
        return dbg(`${path} : 'lastmod' bad date`)
      }
    })

    // all articles should have appropriate status set
    articles.forEach((path) => {
      const file = files[path]
      const ok = ['published', 'archived']
      if (!ok.includes(file.status)) {
        return dbg(`${path} : 'status' bad status`)
      }
    })

    // all articles should have canonical property
    articles.forEach((path) => {
      const file = files[path]
      if (!file.canonicalHost) {
        return dbg(`${path} : 'canonicalHost' not set`)
      }
      if (!/^https:\/\//.test(file.canonical)) {
        return dbg(`${path} : 'canonical' should start with 'https'`)
      }
      if (/\..{0,4}$/.test(file.canonical)) {
        return dbg(`${path} : 'canonical' should not have extension`)
      }
    })

    // all articles should have appropriate tags
    articles.forEach((path) => {
      const file = files[path]
      const ok = [
        'accounting',
        'business',
        'nodejs',
        'linux',
        'tech',
        'xero',
        'photo',
        'productivity'
      ]
      file.tags.forEach((tag) => {
        if (!ok.includes(tag.name)) {
          return dbg(`${path} : ${tag.name} not in list of ok tags`)
        }
      })
    })
  }
}
