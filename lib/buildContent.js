import Metalsmith from 'metalsmith'
import debug from 'debug'
import plugins from './plugins'
import config from 'config'
// import http from 'http'
import debugUi from 'metalsmith-debug-ui'

const dbg = debug(`metalsmith-build-content`)

let metalsmith = Metalsmith('site/content')
if (process.env.NODE_ENV !== 'production') debugUi.patch(metalsmith)
// let metalsmith = Metalsmith(__dirname)
let start = Date.now()
metalsmith.source('')
metalsmith.clean(false)
metalsmith.destination('../../build')
metalsmith.metadata(config.get('meta'))
metalsmith.plugins = plugins

metalsmith.build((err, files) => {
  if (err) return dbg(err)
  // trigger reload
  // http.get('http://localhost:3000/__browser_sync__?method=reload')
  // .on('error', () => dbg('browserSync not listening?'))
  dbg(`build in ${Date.now() - start}ms`)
})
